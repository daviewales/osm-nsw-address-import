#! /usr/bin/env python3

from argparse import ArgumentParser
from pathlib import Path
from sys import exit
import xml.etree.ElementTree as ET


# Functions for manipulating geopandas dataframes
def rstrip_suburb(string, suburb):
    return string[:string.rindex(suburb)]


def remove_invalid_road_names(row, road_names):
    return row[-1] in road_names


def clashing_suburbs(suburb, suburb_list):
    '''Return a list of suburbs which end with the same name as the current suburb
    '''
    return [item for item in suburb_list if item.endswith(suburb) and item != suburb]

# Other functions
def export_geopandas_to_osm_xml(geopandas_data_frame, output_path):
    osm_root = ET.Element('osm', attrib={
        'version': '0.6',
        'generator': 'custom python script'
        })

    i = -1
    for index, row in geopandas_data_frame.iterrows():
        current_node = ET.SubElement(osm_root, 'node', attrib={
            'id': str(i),
            'lat': str(row.geometry.y),
            'lon': str(row.geometry.x),
            'changeset': 'false'})
        for column in geopandas_data_frame.loc[:, ['addr:housenumber', 'addr:street']]:
            ET.SubElement(current_node, 'tag', attrib={
                'k': column,
                'v': row[column]})
        i -= 1
    output_file = ET.ElementTree(element=osm_root)
    output_file.write(output_path)


if __name__ == '__main__':
    parser = ArgumentParser(
            description="""Remove extraneous suburb data and anything lacking address data from geojson file.
            Convert polygons to centroids.""")
    parser.add_argument('suburb', nargs='?', default=None, help='name of suburb to keep (all other suburbs will be removed)')
    parser.add_argument('geojson_file_path', type=Path, metavar='input_path', help='path to the input geojson file')
    parser.add_argument('output_path', type=Path, metavar='output_path', help='output path: automatically determines between geojson or osm format based on extension')
    parser.add_argument('-f', '--force', action='store_true', help='overwrite output file without warning')
    args = parser.parse_args()

    geojson_file_path = args.geojson_file_path
    output_path = args.output_path

    if not geojson_file_path.exists():
        print(f'Error: File {geojson_file_path} does not exist!')
        exit()

    if args.suburb == None:
        suburb = input('What is the name of the suburb? ').upper()
    else:
        suburb = args.suburb.upper()
    print(f'Using suburb name: {suburb}')

    if geojson_file_path.suffix != '.geojson':
        print(f"Warning: File '{geojson_file_path}' does not have the suffix '.geojson'!")
        if input('Are you sure this is a geojson file? (y/[n]): ').lower() not in ['y', 'yes']:
            exit()

    if output_path.suffix not in ['.osm', '.geojson']:
        print(f'Error: output format {output_path.suffix} unrecognised. Acceptable formats are ".osm" or ".geojson"')
        exit()

    import geopandas

    # Find suburbs which we can't remove using naive methods
    all_suburbs = []
    all_suburbs_file = Path('./all_suburbs.csv')
    with open(all_suburbs_file) as file:
        for line in file:
            all_suburbs.append(line.strip())
    problem_suburbs = clashing_suburbs(suburb, all_suburbs)

    # Load file, drop unwanted fields
    geojson_file = geopandas.read_file(geojson_file_path)
    geojson_file.drop(['propid', 'urbanity', 'RID'], inplace=True, axis=1)
    geojson_file.dropna(how='any', inplace=True)

    # Remove problem suburbs
    for problem_suburb in problem_suburbs:
        geojson_file = geojson_file[~geojson_file['address'].str.endswith(problem_suburb)]

    # Now that we have removed all unwanted suburbs which end with the same characters as our suburb,
    # we can easily select *just* our suburb using `endswith`
    geojson_file = geojson_file[geojson_file['address'].str.endswith(suburb)]

    # Remove suburb from address (str.rstrip does not do what you think it does.)
    geojson_file['address'] = geojson_file['address'].apply(rstrip_suburb, args=(suburb,))

    geojson_file['address'] = geojson_file['address'].str.title()
    geojson_file['address'] = geojson_file['address'].str.split(expand=True, n=1)[1]
    geojson_file['address'] = geojson_file['address'].str.strip()
    geojson_file.rename(columns={'housenumber':'addr:housenumber', 'address':'addr:street'}, inplace=True)
    geojson_file['geometry'] = geojson_file['geometry'].centroid

    if output_path.exists() and not args.force:
        if input(f'Warning: output file {output_path} exists! Overwrite? (y/[n]): ').lower() not in ['y', 'yes']:
            exit()

    if output_path.suffix == '.geojson':
        geojson_file.to_file(output_path, driver='GeoJSON')
    elif output_path.suffix == '.osm':
        export_geopandas_to_osm_xml(geojson_file, output_path)


    print(f'Cleaned geojson has been written to {output_path}.')
    print(f'The file contains {len(geojson_file)} addresses.')
    print(f'The last address is {geojson_file.iloc[-1, 0]} {geojson_file.iloc[-1, 1].strip()}.')
